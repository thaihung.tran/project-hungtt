import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import { Blog } from '../shared/models/blog.model';
import { HttpClient } from '@angular/common/http';

const baseUrl = 'https://5f55a98f39221c00167fb11a.mockapi.io/blogs/';

@Injectable()
export class BlogService {
  blogsChanged = new Subject<Blog[]>();

  private blogs: Blog[] = [];

  constructor(protected httpClient: HttpClient) {}


  getAll(params): Observable<any> {
    return this.httpClient.get(baseUrl, { params });
  }

  getBlog(id: number): Observable<any> {
    return this.httpClient.get(baseUrl + `${id}`);
  }
}
