import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BlogsComponent } from './blog.component';
import { BlogsRoutingModule } from './blog-routing.module';
import { SharedModule } from '../shared/shared.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogStartComponent } from './blog-start/blog-start.component';
import { BlogItemComponent } from './blog-list/blog-item/blog-item.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    BlogsComponent,
    BlogListComponent,
    BlogDetailComponent,
    BlogItemComponent,
    BlogStartComponent,
  ],
  imports: [
    RouterModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    BlogsRoutingModule,
    SharedModule,
    FormsModule
  ],
})
export class BlogsModule {}
