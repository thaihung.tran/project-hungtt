import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BlogsComponent } from './blog.component';
import { AuthGuard } from '../auth/auth.guard';

import { BlogStartComponent } from './blog-start/blog-start.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';

const routes: Routes = [
  {
    path: '',
    component: BlogsComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: '', component: BlogStartComponent },
      {
        path: ':id',
        component: BlogDetailComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogsRoutingModule {}
