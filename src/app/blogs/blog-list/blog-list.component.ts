import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Blog } from '../../shared/models/blog.model';
import { BlogService } from '../blog.service';
import { Consts } from 'src/app/shared/constants/consts';
import { BlogSearchFilter } from 'src/app/shared/filters/blog-search.filter';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css'],
})
export class BlogListComponent implements OnInit {
  blogs: Blog[] = [];
  searchFilter: BlogSearchFilter = new BlogSearchFilter();
  title = '';
  content = '';
  sortBy = Consts.SORT_DEFAULT;
  order = Consts.SORT_ASC;
  page = Consts.PAGE_DEFAULT;
  count = 0;
  pageSize = Consts.PAGE_SIZE;

  constructor(
    private blogService: BlogService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getAllBlog();
  }

  getAllBlog() {
    this.setSearchFilter();
    this.blogService.getAll(this.searchFilter).subscribe((response) => {
      this.blogs = response;
      this.count = 57; // total record repsonse
    });
  }

  handlePageChange(event) {
    this.searchFilter.page = this.page = event;
    this.searchFilter.limit = Consts.PAGE_SIZE;
    this.getAllBlog();
  }

  resetData() {
    this.title = '';
    this.content = '';
    this.sortBy = 'createdAt';
    this.order = 'asc';
  }

  setSearchFilter() {
    this.searchFilter.title = this.title;
    this.searchFilter.content = this.content;
    this.searchFilter.sortBy = this.sortBy;
    this.searchFilter.order = this.order;
  }

  search() {
    this.setSearchFilter();
    this.searchFilter.page = this.page = Consts.PAGE_DEFAULT;
    this.searchFilter.limit = 57;
    this.blogService.getAll(this.searchFilter).subscribe((response) => {
      this.blogs = response;
      this.count = this.blogs.length; // total record repsonse
    });
  }
  gotoDetail(item) {
    this.router.navigate(['./', item.id], {relativeTo: this.route});
  }
}
