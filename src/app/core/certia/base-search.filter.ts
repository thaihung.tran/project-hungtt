import { Consts } from 'src/app/shared/constants/consts';

export abstract class BaseSearchFilter {
    page: number;
    limit: number;
    sortBy: string;
    order: string;

    constructor() {
        this.page = Consts.PAGE_DEFAULT;
        this.limit = Consts.PAGE_SIZE;
        this.sortBy = Consts.SORT_DEFAULT; // sort by default createdAt
        this.order = Consts.SORT_ASC; // asc or desc, default asc
    }
}
