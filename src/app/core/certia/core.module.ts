import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthInterceptorService } from '../../auth/auth-interceptor.service';
import { BlogService } from '../../blogs/blog.service';
import { LoaderInterceptor } from '../../shared/interceptors/loader.interceptor';
import { LoaderService } from '../../shared/loading-spinner/loader.service';

@NgModule({
  providers: [
    BlogService,
    LoaderService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptorService,
    //   multi: true
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {}
