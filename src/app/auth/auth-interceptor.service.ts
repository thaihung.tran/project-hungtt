import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler
} from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    let token;
    if (this.authService.user) {
        token = this.authService.user.value.token;
    }

    const accessToken = `Bearer: ${token}`;

    // Add authorization header with access token if available
    if (token) {
        request = request.clone({
            setHeaders: {
                Authorization: accessToken
            }
        });
    }

    return next.handle(request);
  }
}
