import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderService } from './loader.service';

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.css'],
})
export class LoadingSpinnerComponent {
  isLoading: Subject<boolean>;
  constructor(private readonly loaderService: LoaderService) {
    this.isLoading = this.loaderService.isLoading;
  }
}
