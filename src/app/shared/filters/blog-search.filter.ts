import { BaseSearchFilter } from 'src/app/core/certia/base-search.filter';

export class BlogSearchFilter extends BaseSearchFilter {
  public title: string;
  public content: string;

  constructor() {
    super();
    this.title = '';
    this.content = '';
  }
}
