export class Blog {
  public id: string;
  public createdAt: string;
  public title: string;
  public image: string;
  public content: string;

  constructor() {
    this.id = '';
    this.createdAt = '';
    this.title = '';
    this.image = '';
    this.content = '';
  }
}
